/*
  Copyright (c) 2013 Montel Laurent <montel@kde.org>

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef THEMEEDITORMAINWINDOW_H
#define THEMEEDITORMAINWINDOW_H

#include <KXmlGuiWindow>

class ThemeEditorPage;
class KAction;

class ThemeEditorMainWindow : public KXmlGuiWindow
{
    Q_OBJECT
public:
    explicit ThemeEditorMainWindow();
    ~ThemeEditorMainWindow();

protected:
    void closeEvent(QCloseEvent *);

private Q_SLOTS:
    void slotNewTheme();
    void slotCloseTheme();
    void slotAddExtraPage();
    void slotOpenTheme();
    void slotUploadTheme();

private:
    void updateActions();
    void saveCurrentProject(bool createNewTheme);
    void setupActions();
    ThemeEditorPage *mThemeEditor;
    KAction *mNewThemeAction;
    KAction *mCloseThemeAction;
    KAction *mAddExtraPage;
    KAction *mCloseAction;
    KAction *mOpenAction;
    KAction *mUploadTheme;
};

#endif // THEMEEDITORMAINWINDOW_H
