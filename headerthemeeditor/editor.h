/*
  Copyright (c) 2013 Montel Laurent <montel@kde.org>

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef EDITOR_H
#define EDITOR_H

#include <KTextEdit>

class QCompleter;

class Editor : public KTextEdit
{
    Q_OBJECT
public:
    explicit Editor(QWidget *parent = 0);
    ~Editor();

private Q_SLOTS:
    void slotInsertCompletion( const QString &completion );

protected:
    void keyPressEvent(QKeyEvent* e);
private:
    void initCompleter();
    QString wordUnderCursor() const;
    QCompleter *m_completer;
};

#endif // EDITOR_H
